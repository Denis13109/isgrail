# Lancement
Exécuter le .jar qui est dans la racine du projet.

## 1. Prologue
- Mise en situation de l'histoire du jeux
- Entrez le nom de son personnage

## 2. Premiers pas dans le monde d'Isgrail
- Vous pouvez regarder la liste du stuff déblocable pendant la partie 
- Et après celà avancer dans ce monde magique

## 3. Suite de l'aventure

- Avancer dans les différentes parties du monde, vous pouvez attaquer des montres, rechercher du stuff ou encore fuir devant des monstres trop puissants !!
- Attention à toujours garder de la vie
- Votre niveau augmentera d'un chaque fois que la barre d'expérience sera au maximum, vous serez donc plus puissant !
- Le but final est d'arrivé à la centième partie du monde magique d'Isgrail.