package net.monster;

import net.monster.interfacemonster.IntDryadActions;
import net.player.Personnage;
import net.random.Random;

public class Dryad extends Monster implements IntDryadActions{
	
public Dryad() {
		
	}
	
	public Dryad(int life, int attack) {
		this.life = life;
		this.attack = attack;
	}
	
	@Override
	public boolean attackTarget(Personnage target) {
		Random random = new Random();
		int randomAttack = random.generateRandomNumber(0, attack);
		if(randomAttack == 0) {
			return false;
		}
		target.setLife(target.getLife() - randomAttack);
		return true;
		
	}

	@Override
	public void root() {
		// TODO Auto-generated method stub
		/*
		 * M�thode sp�ciale obtenue gr�ce � la classe abstraite.
		 * M�thode unique � la dryad et pas aux monstres gr�ce � l'impl�mentation.
		 */
		System.out.println("Le joueur est fig�");
		
	}

}
