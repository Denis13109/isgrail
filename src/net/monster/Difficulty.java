package net.monster;

import net.map.Map;

public class Difficulty {

	private int level = 0;
	
	
	public Difficulty() {
		
	}


	public int getLevel() {
		return level;
	}


	public void setLevel(int level, Map isgrailMap) {
		this.level = level;
		switch(level) {
			case 1: 
				setDifficulty(isgrailMap,0,3,1,5,1,3,1,1,1,1,6,0,4,0,5,25);
				break;
			case 2: 
				setDifficulty(isgrailMap,0,3,3,6,1,4,2,1,2,1,6,0,3,0,5,30);
				break;
			case 3: 
				setDifficulty(isgrailMap,0,2,4,10,2,4,2,2,2,2,5,0,3,0,4,35);
				break;
			case 4: 
				setDifficulty(isgrailMap,0,2,5,15,3,7,2,2,2,2,4,0,2,1,4,45);
				break;
			case 5: 
				setDifficulty(isgrailMap,0,1,7,20,4,8,2,2,2,2,4,0,2,1,3,55);
				break;
			default:
		
		}
	}
	
	public void setDifficulty(Map isgrailMap,int minSpawnMonster, int maxSpawnMonster,
			int minSpawnMonsterLife,int maxSpawnMonsterLife,int minSpawnMonsterAttack,
			int maxSpawnMonsterAttack, int maxItemsAttack, int minItemsAttack, int maxItemsLife,
			int minItemsLife, int maxSpawnItems, int minSpawnItems, int maxLootPotion, int minLootPotion,
			int maxHealPotion, int maxEperienceAgainMonster) {
		
		isgrailMap.setMaxSpawnMonster(maxSpawnMonster);
		isgrailMap.setMaxSpawnMonsterAttack(maxSpawnMonsterAttack);
		isgrailMap.setMaxSpawnMonsterLife(maxSpawnMonsterLife);
		isgrailMap.setMinSpawnMonster(minSpawnMonster);
		isgrailMap.setMinSpawnMonsterLife(minSpawnMonsterLife);
		isgrailMap.setMinSpawnMonsterAttack(minSpawnMonsterAttack);
		
		isgrailMap.setMaxItemsAttack(maxItemsAttack);
		isgrailMap.setMaxItemsLife(maxItemsLife);
		isgrailMap.setMaxSpawnItems(maxSpawnItems);
		isgrailMap.setMinItemsAttack(minItemsAttack);
		isgrailMap.setMinItemsLife(minItemsLife);
		isgrailMap.setMinSpawnItems(minSpawnItems);
		
		isgrailMap.setMaxLootPotion(maxLootPotion);
		isgrailMap.setMinLootPotion(minLootPotion);
		isgrailMap.setMaxHealPotion(maxHealPotion);
		isgrailMap.setMaxEperienceAgainMonster(maxEperienceAgainMonster);
	}

}
