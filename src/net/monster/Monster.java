package net.monster;

import net.player.Personnage;
import net.random.Random;

public abstract class Monster {

	protected int life;
	protected int attack;
	
	public abstract boolean attackTarget(Personnage target);

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}
}
