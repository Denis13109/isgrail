package net.monster;

import net.monster.interfacemonster.IntSpecialsActions;
import net.player.Personnage;
import net.random.Random;

public class Medusa extends Monster implements IntSpecialsActions{
public Medusa() {
		
	}
	
	public Medusa(int life, int attack) {
		this.life = life;
		this.attack = attack;
	}
	
	@Override
	public boolean attackTarget(Personnage target) {
		Random random = new Random();
		int randomAttack = random.generateRandomNumber(0, attack);
		if(randomAttack == 0) {
			return false;
		}
		target.setLife(target.getLife() - randomAttack);
		return true;
		
	}

	@Override
	public void petrify() {
		/*
		 * M�thode sp�ciale obtenue gr�ce � la classe abstraite.
		 * M�thode unique � medusa et pas aux monstres gr�ce � l'impl�mentation.
		 */
		System.out.println("Medusa vient de p�trifier le joueur");
		
	}
}
