package net.monster;

import net.player.Personnage;
import net.random.Random;

public class Minotaur extends Monster{
	
public Minotaur() {
		
	}
	
	public Minotaur(int life, int attack) {
		this.life = life;
		this.attack = attack;
	}
	
	@Override
	public boolean attackTarget(Personnage target) {
		Random random = new Random();
		int randomAttack = random.generateRandomNumber(0, attack);
		if(randomAttack == 0) {
			return false;
		}
		target.setLife(target.getLife() - randomAttack);
		return true;
		
	}

}
