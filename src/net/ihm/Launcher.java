package net.ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import net.config.Config;
import net.history.History;

public class Launcher {

	public Launcher() {
		/*
		 * Constructer appell� dans le main.
		 * Ici on vient cr�er la premi�re interface graphique. On y d�couvre l'histoire
		 * Un bouton suivant est propos� pour faire afficher diff�rents type de textes.
		 */
		History history = new History();
		Config config = new Config();
		Clip launcherClip = config.displayAudio(config.getAudioPathLauncher());
		JFrame frameHistory = new JFrame("History");
		
		JTextField tfPseudo = new JTextField();
	    tfPseudo.setBounds(150,250, 130,20);
	    JTextArea txtHistory = new JTextArea(5, 20);
	    txtHistory.setText(history.getHistory1());
	    txtHistory.setEditable(false);
        JScrollPane scroll = new JScrollPane(txtHistory);
        config.setImage(frameHistory, config.getImagePath());
        
        JButton buttonNextHistory=new JButton("Suivant");
	    JButton buttonValidateHistory=new JButton("Valider");
	    JButton buttonStartHistory=new JButton("Start");
        
        buttonNextHistory.setBounds(150,200,95,30);
	    buttonValidateHistory.setBounds(50,200,95,30);
	    buttonStartHistory.setBounds(150,200,95,30);
	    
        buttonValidateHistory.setEnabled(false);
	    buttonValidateHistory.setVisible(false);
	    buttonStartHistory.setEnabled(false);
	    buttonStartHistory.setVisible(false);
	    tfPseudo.setEnabled(false);
	    tfPseudo.setVisible(false);
	    
	    buttonNextHistory.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){ 
	    			history.setNbHistory(history.getNbHistory() + 1);
	    			switch(history.getNbHistory()) {
	    			  case 1:
	    				  txtHistory.setText(history.getHistory1());
	    			    break;
	    			  case 2:
	    				  txtHistory.setText(history.getHistory2());
	    			    break;
	    			  case 3:
	    				  txtHistory.setText(history.getHistory3());
	    				  buttonValidateHistory.setEnabled(true);
	    				  buttonValidateHistory.setVisible(true);
	    				  tfPseudo.setEnabled(true);
	    				  tfPseudo.setVisible(true);
	    				  buttonNextHistory.setEnabled(false);
	    			    break;
	    			  case 4:
	    				  txtHistory.setText(history.getHistory4());
	    			    break;
	    			  case 5:
	    				  txtHistory.setText(history.getHistory5());
	    			    break;
	    			  case 6:
	    				  txtHistory.setText(history.getHistory6());
	    			    break;
	    			  case 7:
	    				  txtHistory.setText(history.getHistory7());
	    			    break;
	    			  case 8:
	    				  txtHistory.setText(history.getHistory8());
	    			    break;
	    			  case 9:
	    				  txtHistory.setText(history.getHistory9());
	    			    break;
	    			  case 10:
	    				  txtHistory.setText(history.getInformations());
	    				  buttonNextHistory.setEnabled(false);
	    				  buttonNextHistory.setVisible(false);
	    				  buttonStartHistory.setEnabled(true);
	    				  buttonStartHistory.setVisible(true);
	    				  
	    			    break;
	    			  default:
	    			    // code block
	    			}
	    	          
	    	        }  
	    	    });
	    buttonValidateHistory.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){
	    		String nameChosen = tfPseudo.getText();
	    		if(nameChosen.length() < 3) {
	    			txtHistory.setText(history.getWrongName());
	    		}else {
	    			tfPseudo.setEnabled(false);
	    			txtHistory.setText("Aaah je vois ! Enchant�e " + nameChosen + " ! \n"
	    					+ history.getHistory3b());
	    			tfPseudo.setVisible(false);
	    			tfPseudo.setEnabled(false);
	    			buttonValidateHistory.setEnabled(false);
	    			buttonValidateHistory.setVisible(false);
  				    buttonNextHistory.setEnabled(true);
	    			
	    		}
	    		
	    	        }  
	    	    });
	    
	    buttonStartHistory.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){  
	    		frameHistory.setEnabled(false);
	    		frameHistory.setVisible(false);
	    		launcherClip.stop();
				World world = new World(tfPseudo.getText());
	    	        }  
	    	    });
	    frameHistory.addWindowListener(new java.awt.event.WindowAdapter() {
	        public void windowClosing(WindowEvent winEvt) {
	            System.exit(0);
	        }
	    });
	    
	    scroll.setBounds(50, 25, 400, 150);
        frameHistory.add(scroll);
        frameHistory.add(buttonNextHistory);
        frameHistory.add(buttonValidateHistory);
        frameHistory.add(buttonStartHistory);
        frameHistory.add(tfPseudo);
        frameHistory.pack();
	    frameHistory.setSize(500,450);  
	    frameHistory.setLayout(null);
	    frameHistory.setVisible(true);
	}
	
	

}
