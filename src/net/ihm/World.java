package net.ihm;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import net.random.Random;
import threads.MyThread;

import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import net.config.Config;
import net.map.Map;
import net.monster.Difficulty;
import net.monster.Monster;
import net.object.Potion;
import net.object.Stuff;
import net.player.Personnage;

public class World {
	
	private boolean isChecked=false;
	private Monster actualMonster = null;
	private Clip background = null;
	private int startXpPlayer = 150;
	private JTextArea txtWorld = new JTextArea(5, 20);
	
	public JTextArea getTextAreaWorld() {
		return txtWorld;
	}

	public void setTextAreaWorld(JTextArea txtWorld) {
		this.txtWorld = txtWorld;
	}
	
	public World() {
		
	}
	
	public World(String playerName) {
		
		JFrame frame=new JFrame("Isgra�l");
		Personnage player = new Personnage(playerName,15 , 3, 1);
		player.setMaxEperience(150);
		Map isgrailMap = new Map();
		Config config = new Config();
		config.setImage(frame, config.getImagePathMap());
		background = config.displayAudio(config.getAudioPathLOTRSnes());
		Difficulty difficulty = new Difficulty();
		difficulty.setLevel(1, isgrailMap);
		
		ihm(frame,isgrailMap, player,config);
		
	}
	
	public void ihm(JFrame frame, Map isgrailMap, Personnage player, Config config) {
		
		JButton buttonAttack=new JButton("Attaquer"); // Ici le bouton attaquer
	    JButton buttonFlee=new JButton("Fuire"); // Ici le bouton fuire
	    JButton buttonSearch=new JButton("Chercher"); // Ici le bouton chercher
	    JButton buttonMoveFoward=new JButton("Avancer"); // Ici le bouton Avancer
	    JButton buttonStuff=new JButton("Stuff?"); // Ici le bouton Stuff?
	    
	    buttonAttack.setEnabled(false); // On d�sactive les boutons pour le d�but
	    buttonFlee.setEnabled(false);
	    buttonSearch.setEnabled(false);
	    buttonStuff.setEnabled(true);
	    
	    final JTextField tfNumberMap=new JTextField();  // Zone de text pour
	    tfNumberMap.setBounds(900,50, 100,20);
	    tfNumberMap.setEditable(false); // D�sactiver le edit, le joueur n'en a pas besoin
	    getTextAreaWorld().setBounds(810,350, 300,400);
	    getTextAreaWorld().setEditable(false);
	    getTextAreaWorld().setText("Bienvenue dans le monde d'Isgra�l !");
        JScrollPane scrollWorld = new JScrollPane(txtWorld);
	    
        
        // **** Tous les labels **** 
	    JLabel labelMap = new JLabel("Votre position dans Isgra�l");
	    labelMap.setBounds(875,20, 300,20);
	    JLabel labelWorld = new JLabel("Monde :");
	    labelWorld.setBounds(930,320, 300,20);
	    JLabel labelPlayer = new JLabel("Personnage :");
	    labelPlayer.setBounds(40,70, 300,40);
	    labelPlayer.setForeground(Color.GRAY);
	    labelPlayer.setFont (labelPlayer.getFont ().deriveFont (30.0f));
	    JLabel labelPlayerName = new JLabel("Nom :");
	    labelPlayerName.setBounds(20,110, 300,20);
	    labelPlayerName.setForeground(Color.green);
	    labelPlayerName.setFont (labelPlayerName.getFont ().deriveFont (20.0f));
	    JLabel labelPlayerNameValue = new JLabel(player.getName());
	    labelPlayerNameValue.setBounds(120,110, 300,20);
	    labelPlayerNameValue.setForeground(Color.green);
	    labelPlayerNameValue.setFont (labelPlayerNameValue.getFont ().deriveFont (20.0f));
	    JLabel labelPlayerLife = new JLabel("Vie :");
	    labelPlayerLife.setBounds(20,140, 300,20);
	    labelPlayerLife.setForeground(Color.red);
	    labelPlayerLife.setFont (labelPlayerLife.getFont ().deriveFont (20.0f));
	    JLabel labelPlayerLifeValue = new JLabel(String.valueOf(player.getLife()) + " / " + String.valueOf(player.getMaxLife()));
	    labelPlayerLifeValue.setBounds(120,140, 300,20);
	    labelPlayerLifeValue.setForeground(Color.red);
	    labelPlayerLifeValue.setFont (labelPlayerLifeValue.getFont ().deriveFont (20.0f));
	    JLabel labelPlayerAttack = new JLabel("Attaque :");
	    labelPlayerAttack.setBounds(20,170, 300,20);
	    labelPlayerAttack.setForeground(Color.blue);
	    labelPlayerAttack.setFont (labelPlayerAttack.getFont ().deriveFont (20.0f));
	    JLabel labelPlayerAttackValue = new JLabel(String.valueOf(player.getAttack()));
	    labelPlayerAttackValue.setBounds(120,170, 300,20);
	    labelPlayerAttackValue.setForeground(Color.blue);
	    labelPlayerAttackValue.setFont (labelPlayerAttackValue.getFont ().deriveFont (20.0f));
	    
	    JLabel labelPlayerLevel = new JLabel("Niveau : ");
	    labelPlayerLevel.setBounds(20,200, 300,20);
	    labelPlayerLevel.setForeground(Color.blue);
	    labelPlayerLevel.setFont (labelPlayerLevel.getFont ().deriveFont (20.0f));
	    JLabel labelPlayerLevelValue = new JLabel(String.valueOf(player.getLevel()));
	    labelPlayerLevelValue.setBounds(120,200, 300,20);
	    labelPlayerLevelValue.setForeground(Color.blue);
	    labelPlayerLevelValue.setFont (labelPlayerLevelValue.getFont ().deriveFont (20.0f));
	    JLabel labelPlayerExperience = new JLabel("Exp�rience : ");
	    labelPlayerExperience.setBounds(20,230, 300,20);
	    labelPlayerExperience.setForeground(Color.blue);
	    labelPlayerExperience.setFont (labelPlayerAttackValue.getFont ().deriveFont (20.0f));
	    JLabel labelPlayerExperienceValue = new JLabel(String.valueOf(player.getExperience()) + " / " + String.valueOf(player.getMaxEperience()) );
	    labelPlayerExperienceValue.setBounds(150,230, 300,20);
	    labelPlayerExperienceValue.setForeground(Color.blue);
	    labelPlayerExperienceValue.setFont (labelPlayerAttackValue.getFont ().deriveFont (20.0f));
	    JLabel labelImagePlayer = new JLabel();
	    labelImagePlayer.setBounds(20,260, 300,500);
	    JLabel labelImageMonster = new JLabel();
	    labelImageMonster.setBounds(500,270, 300,500);
	    // **** Tous les labels **** END
	    
	    buttonAttack.setBounds(50,560,120,60); 
	    buttonFlee.setBounds(50,640,120,60);
	    buttonSearch.setBounds(180,640,120,60); 
	    buttonMoveFoward.setBounds(180,560,120,60); 
	    buttonStuff.setBounds(310,560,120,60); 
	    
	    ImageIcon playerImg = new ImageIcon(config.getImagePathPlayer());
	    labelImagePlayer.setIcon(playerImg);
	    
	    
	    buttonAttack.addActionListener(new ActionListener(){  
	public void actionPerformed(ActionEvent e){  
		/*
		 * Methode appell�e lorsque l'on clique sur le boutton attaquer
		 * On affiche le texte retourn� par la m�thode Combat ( M�thode qui 
		 * g�n�re un nombre al�atoire. Selon ce nombre, le monstre a l'initiative
		 * ou le joueur a l'initiative. Et les deux s'attaquent mutuellement pour 1 tour.
		 * Cette m�thode g�re la mort du monstre ainsi que le la mort du joueur. Si l'un
		 * des deux vient � mourir, des actions sont pr�par�es pour cette occasion.
		 * Pour terminer, on utilise la m�thode majCaracPlayer qui vient remettre � jour
		 * la vie du joueur graphiquement (sinon les caract�ristiques graphiques ne sont pas
		 * dynamiques, et ne se mettent pas � jour toutes seules.
		 */
		config.displayAudio(config.getAudioPathSword());
		txtWorld.setText(combat(player, buttonMoveFoward, buttonSearch, buttonFlee, buttonAttack, isgrailMap, labelPlayerLifeValue, labelImageMonster, frame,config,txtWorld));
		majCaracPlayer(player, labelPlayerLifeValue, labelPlayerAttackValue, labelPlayerLevelValue,labelPlayerExperienceValue);
		
	}  
	    });
	    
	    buttonFlee.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){
	    		
	    		/*
	    		 * M�thode appell�e lorsque l'on clique sur le bouton 'fuire'
	    		 * Appelle la m�thode tryFlee, qui fait fuire le joueur avec 1 chance sur 3.
	    		 * Display les bon audios s'il y a fuite ou non.
	    		 * Try to flee retourne FALSE si le joueur ne s'enfuit pas.
	    		 * Et met � jour les boutons appropri�s.
	    		 */
	    		boolean isFlee = tryFlee();
	    		if(isFlee) {
	    			txtWorld.setText("Tu viens de fuire");
	    			labelImageMonster.setVisible(false);
		    		config.displayAudio(config.getAudioPathWalk());
		    		buttonMoveFoward.setEnabled(true);
		    		if(!isChecked) {
		    		buttonSearch.setEnabled(true);
		    		}
		    		buttonFlee.setEnabled(false);
		    		buttonAttack.setEnabled(false);
	    		}else {
	    			txtWorld.setText("La fuite est impossible !");
	    			config.displayAudio(config.getAudioPathGrrr());
	    			buttonFlee.setEnabled(false);
	    		}
	    		
	    	        }  
	    	    });
	    
	    buttonSearch.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){  
	    		/*
	    		 * M�thode appell�e lorsque l'on clique sur le bouton 'chercher'.
	    		 * En fonction de la difficult� et le nombre de map, on a x chance
	    		 * de trouver un item. 
	    		 * La m�thode stuff.spawn g�n�re un item ou non.
	    		 * Les caract�ristiques graphiques sont remises � jour si il y a
	    		 * un item trouv� et �quip�.
	    		 * En cherchant un objet, cela on peut tomber sur un monstre.
	    		 * La m�thode HasSpawn qui est appell� g�n�re un monstre sur de l'al�atoir.
	    		 * StateButton remet � jour les bouttons, en fonction de s'il y a un monstre ou
	    		 * non. 
	    		 */
	    		 
	    		config.displayAudio(config.getAudioPathSearch());
	    		isChecked = true;
	    		Stuff newStuff = isgrailMap.stuffSpawn();
	    		trySearch(newStuff,txtWorld,player);
	    		majCaracPlayer(player, labelPlayerLifeValue, labelPlayerAttackValue, labelPlayerLevelValue,labelPlayerExperienceValue);
    			Monster newMonster = isgrailMap.spawn(labelImageMonster, config);
				monsterHasSpawn(newMonster, txtWorld);
				stateButton(isgrailMap, buttonMoveFoward, buttonSearch, buttonFlee, buttonAttack,newMonster);
	            tfNumberMap.setText(String.valueOf(isgrailMap.getPositionPlayer()) + "/" + isgrailMap.getMAPTOT());
	    		buttonSearch.setEnabled(false);
	        }  
	    	    });
	    
	    buttonMoveFoward.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){
	    		/*
	    		 * Methode appell�e lorsque l'on clique sur le bouton 'avancer'
	    		 * Met le bruitage de d�placement.
	    		 * Remet � jour le num�ro de map.
	    		 * Utilise la m�thode .spawnpour essayer de faire spawn un monstre sur de l'al�atoire
	    		 * Mets la boutons � jour si un monstre a spawn ou non grace � la m�thode
	    		 * StateButton.
	    		 */
	    				isChecked = false;
	    				config.displayAudio(config.getAudioPathFoward());
	    				isgrailMap.setPositionPlayer(isgrailMap.getPositionPlayer() + 1);
	    				txtWorld.setText("Tu viens d'avancer dans le monde d'Isgra�l..\n"
	    	            		+ "Tu es sur la map n�" + isgrailMap.getPositionPlayer() + numberOfDifficulty(isgrailMap));
	    				Monster newMonster = isgrailMap.spawn(labelImageMonster, config);
	    				monsterHasSpawn(newMonster, txtWorld);
	    				stateButton(isgrailMap, buttonMoveFoward, buttonSearch, buttonFlee, buttonAttack,newMonster);
	    	            tfNumberMap.setText(String.valueOf(isgrailMap.getPositionPlayer()) + "/" + isgrailMap.getMAPTOT());
	    	           /*
	    	            * Ici la partie de la victoire ! 
	    	            * Si le joueur arrive � la case 100
	    	            */
	    	            if(isgrailMap.getPositionPlayer() == 100) {
	    	            
	    	            	txtWorld.setText("Tu as gagn� !! Soyons sadiques et recommence � nouveau !");
	    	            	isgrailMap.setPositionPlayer(1);
	    	            }
	    	}  
	    	    });
	    
	    frame.addWindowListener(new java.awt.event.WindowAdapter() {
	        public void windowClosing(WindowEvent winEvt) {
	        	/*
	        	 * M�thode appell�e lorsque l'on ferme l'application. Permet de tout fermer
	        	 * correctement. Surtout s'il y a de la musique ou des process en cours.
	        	 */
	            System.exit(0);
	        }
	    });
	    buttonStuff.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){  
	    		MyThread thread = new MyThread();
	    		thread.myInfosSetted(txtWorld);
	    		thread.run();
	        }  
	    	    });
	    
	    
	    //AJOUT des components � l'UI
	    frame.add(buttonAttack);
	    frame.add(buttonFlee);
	    frame.add(buttonSearch);
	    frame.add(buttonMoveFoward);
	    frame.add(buttonStuff);
	    frame.add(tfNumberMap);
	    frame.add(labelMap);
	    frame.add(labelWorld);
	    frame.add(txtWorld);
	    frame.add(scrollWorld);
	    frame.add(labelPlayer);
	    frame.add(labelPlayerName);
	    frame.add(labelPlayerNameValue);
	    frame.add(labelPlayerLife);
	    frame.add(labelPlayerLifeValue);
	    frame.add(labelPlayerAttackValue);
	    frame.add(labelPlayerAttack);
	    frame.add(labelImagePlayer);
	    frame.add(labelImageMonster);
	    frame.add(labelPlayerExperienceValue);
	    frame.add(labelPlayerExperience);
	    frame.add(labelPlayerLevelValue);
	    frame.add(labelPlayerLevel);
	    
	    frame.setSize(1200,1080);  
	    frame.setLayout(null);
	    frame.setEnabled(true);
	    frame.setVisible(true);
		
		
	}

	public void stateButton(Map isgrailMap, JButton foward , JButton search, JButton flee,JButton attack, Monster newMonster) {
		
		/*
		 * M�thode qui met � jour les boutons en fonction de plusieurs cas :
		 * Si pas de monstres alors -> Avancer ou Fouiller.
		 * Si isChecked est == true (Si le joueur � d�j� fouill� la map), mettre le button fouiller
		 * en Disable..
		 */
		if(isgrailMap.getPositionPlayer() ==1) {
			
			foward.setEnabled(true);
			search.setEnabled(true);
			flee.setEnabled(true);
			attack.setEnabled(true);
			
		}
		
		if(newMonster == null) {
			
			attack.setEnabled(false);
			flee.setEnabled(false);
			if(!isChecked) {
				search.setEnabled(true);
			}
			foward.setEnabled(true);
			
		}else {
			attack.setEnabled(true);
			flee.setEnabled(true);
			search.setEnabled(false);
			foward.setEnabled(false);
		}
		
	}
	
	public void monsterHasSpawn(Monster newMonster, JTextArea txt) {
		
		/*
		 * M�thode qui affiche du texte en fonction de l'apparition de monstres ou non.
		 * Si monstre == null, cela veut dire qu'aucun monstre n'a �t� g�n�r�.
		 */
		
		if(newMonster == null) {
			txt.append("\n Ouf qu'elle chance....\n"
					+ "Aucun monstre � l'horizon !");
		}else {
			txt.append("\n Aaah ! Un monstre vient d'appara�tre !\n"
					+ "Tu le reconnais, c'est un(e) : "+newMonster.getClass()+"\n"
							+ "Incroyable, "+newMonster.getClass()+" a "+ newMonster.getLife()+" points de vie\n"
									+ "et "+newMonster.getAttack()+" points d'attaque !\n"
					+ "Que vas tu faire ?");
			actualMonster = newMonster;
			
		}
		
	}
	
	public String numberOfDifficulty(Map isgrailMap){
		
		/*
		 * M�thode qui g�n�re du texte en fonction du nombre de map.
		 * Si : map 0-24, difficult� mise en facile
		 * map 25-54, difficult� mise en normal
		 * map 55-75, difficult� mise en hard
		 * map 75-94, difficult� mise en hardCore
		 * map 95 et + difficult� mise en extr�me.
		 * La classe difficulty met beaucoup de param�tres � jour en fonction du level de difficult�
		 * Comme la vie des monstres de 1 � 6 ne sera pas la m�me en extr�me. Mais aussi l'attaque,
		 * l'xp g�n�r�e ect...
		 */
		
		switch(isgrailMap.getPositionPlayer()) {
		
		case 1:
			Difficulty difficulty1 =  new Difficulty();
			difficulty1.setLevel(1, isgrailMap);
			return "\n Attention, la difficult� est en 'Facile'";
			
		case 25:
			Difficulty difficulty2 =  new Difficulty();
			difficulty2.setLevel(1, isgrailMap);
			return "\n Attention, la difficult� est en 'Normal'";
		case 50:
			Difficulty difficulty3 =  new Difficulty();
			difficulty3.setLevel(1, isgrailMap);
			return "\n Attention, la difficult� est en 'Hard'";
		case 75:
			Difficulty difficulty4 =  new Difficulty();
			difficulty4.setLevel(1, isgrailMap);
			return "\n Attention, la difficult� est en 'HardCore +'";
		case 95:
			Difficulty difficulty5 =  new Difficulty();
			difficulty5.setLevel(1, isgrailMap);
			return "\n Attention, la difficult� est en 'extr�me'";
			default:
				return"";
		}
		
		
		
	}
	
	public boolean tryFlee() {
		/*
		 * M�thode qui retourne true si on fuit.
		 * False si 'fuite impossible'
		 * On utilise notre propre classe random qui poss�de la m�thode generateRandomNumber. Cette
		 * M�thode nous g�n�re un nombre al�atoire entre un min, max, et retourne le nombre sous 'int'.
		 */
		Random random = new Random();
		int randomNumber = random.generateRandomNumber(0, 1);
		if(randomNumber == 1) {
			return true;
		}else {
			return false;
		}
		
		
	}
	
	public void trySearch(Stuff newStuff, JTextArea txt, Personnage player) {
		
		/*
		 * M�thode qui v�rifie l'objet newStuff (Objet g�n�r� sur la map)
		 * Si newStuff est nul, alors pas de stuff trouv� sur la map. En revanche, si un item
		 * Est trouv�, on g�n�re du texte et on modifie les caract�ristiques du joueur..
		 * (Les items augmentes al�atoirement les caracs du joeueur en fonction de la difficult�.
		 */
		if(newStuff == null) {
			txt.setText("Tu n'as rien trouv� ...");
		}else {
			txt.setText("Un objet � �t� trouv� lors de\n"
					+ "ta recherche, il s'agit de: " + newStuff.getName() + " \n"
							+ "Super ! Il te permet d'avoir " + newStuff.getLife() + " pv en plus \n"
									+ "ainsi que " + newStuff.getAttack() + " D'attaque en plus ! \n");
			player.setAttack(player.getAttack() + newStuff.getAttack());
			player.setMaxLife(player.getMaxLife() + newStuff.getLife());
			//generation d'un objet
		}
	}
	
	public String combat(Personnage player, JButton buttonMoveFoward, JButton buttonSearch, JButton buttonFlee, JButton buttonAttack, Map isgrailMap, JLabel labelPlayerLifeValue, JLabel labelImgMonster, JFrame frame,Config config, JTextArea txt) {
		
		/*
		 * M�thode combat, qui fait attaquer le joueur ainsi que le monstre en question.
		 * Les caract�ristiques sont remises � jour respectivement pour les deux.
		 * Les logs du combats sont retourn�es, afin de les faires afficher plus tard.
		 * Elles sont toutes regroup�es puis retourn�es.
		 */
		
		Random random = new Random();
		String logsCombats = "";
		int initiative = random.generateRandomNumber(1, 3);
		if(initiative == 1) {
			//Ici le monstre � l'initiative
			logsCombats = "\n Rah .. Le monstre � l'initiative !";
			logsCombats = logsCombats + monsterAttackPlayer(player, actualMonster, labelPlayerLifeValue,frame, config);
			logsCombats = logsCombats + playerAttackMonster(player, actualMonster, buttonMoveFoward, buttonSearch, buttonFlee, buttonAttack, isgrailMap,labelImgMonster, config, txt);
			return logsCombats;
		}
		// Ici, le joueur � l'initiative
		logsCombats = "\n Super ! Tu as l'initiative de l'attaque !";
		logsCombats = logsCombats + playerAttackMonster(player, actualMonster, buttonMoveFoward, buttonSearch, buttonFlee, buttonAttack, isgrailMap,labelImgMonster, config,txt);
		logsCombats = logsCombats + monsterAttackPlayer(player, actualMonster, labelPlayerLifeValue,frame,config);
		return logsCombats;
		
		
	}
	
	public String playerAttackMonster(Personnage player, Monster actualMonster, JButton buttonMoveFoward, JButton buttonSearch, JButton buttonFlee, JButton buttonAttack, Map isgrailMap, JLabel labelImgMonster, Config config, JTextArea txt) {
		/*
		 * M�thode qui permet au joueur d'attaquer le monstre.
		 * En fonction du r�sultat, de l'audio est g�n�r� ainsi que du texte appropri� est retourn�.
		 * Si le monstre meurt, on met la variable du monstre � null pour siginifier qu'il est mort.
		 */
		String logsCombats = "";
		boolean isAttacked = player.attackMonster(actualMonster);
		if(!isAttacked) {
			logsCombats = logsCombats + " \n Mince .. Tu as manqu� \n"
					+ "ta cible.. Grrr \n" ;
		}else {
			if(actualMonster.getLife() <= 0) {
				actualMonster = null ; // dead, delete object
				Random random = new Random();
				stateButton(isgrailMap, buttonMoveFoward, buttonSearch, buttonFlee, buttonAttack,null);
				labelImgMonster.setVisible(false);
				player.getExeperienced(player, random.generateRandomNumber(1, isgrailMap.getMaxEperienceAgainMonster()), txt);
				return logsCombats = logsCombats + "\n Le monstre est mort ! Bravo" + isLoot(player, isgrailMap, config);
			}
			logsCombats = logsCombats + "\n Tu l'as touch� ! Voici ses PV : " + actualMonster.getLife();
		}
		return logsCombats;
	}
	
	public String monsterAttackPlayer(Personnage player, Monster actualMonster, JLabel labelPlayerLifeValue, JFrame frame, Config config) {
		
		/*
		 * M�thode qui permet au monstre d'attaquer le joueur.
		 * En fonction du r�sultat, de l'audio est g�n�r� ainsi que du texte appropri� est retourn�.
		 * Si le Joueur meurt, l'interface se ferme.
		 */
		String logsCombats = "";
		int playerLifeBefore = player.getLife();
		Boolean isPlayerAttacked = actualMonster.attackTarget(player);
		if(!isPlayerAttacked) {
			config.displayAudio(config.getAudioPathParade());
			return " \n Quelle chance, \n"
					+ "Tu as par� l'attaque du monstre !\n";
		}else {
			//Ici des sons randoms pour les d�gats subits par le monstre
			//Nombre entre 1 et 6
			Random random = new Random();
			int soundHurtRandom = random.generateRandomNumber(1, 6);
			switch(soundHurtRandom) {
			case 1:
				config.displayAudio(config.getAudioPathHurtMale1());
				break;
			case 2:
				config.displayAudio(config.getAudioPathHurtMale2());
				break;
			case 3:
				config.displayAudio(config.getAudioPathHurtMale3());
				break;
			case 4:
				config.displayAudio(config.getAudioPathHurtMale4());
				break;
			case 5:
				config.displayAudio(config.getAudioPathHurtMale5());
				break;
			case 6:
				config.displayAudio(config.getAudioPathHurtMale6());
				break;
				default:
					
			}
			logsCombats ="\n Le monstre t'a touch�, tu as \n"
					+ "perdu des pv : " + (playerLifeBefore - player.getLife() + "\n");
			labelPlayerLifeValue.setText(String.valueOf(player.getLife()));
			if(player.getLife() <= 0) {
				logsCombats = logsCombats + "\n Tu es mort ....";
				background.stop();
				GameOver gameOver = new GameOver(frame);
				
			}
			return logsCombats;
		}
		
	}
	
	public void majCaracPlayer(Personnage player, JLabel labelPlayerLifeValue, JLabel labelPlayerAttackValue, JLabel labelPlayerLevelValue, JLabel labelPlayerExperienceValue) {
		
		/*
		 * M�thode qui met � jour graphiquement les caract�ristiques du joueur.
		 * On donne les labels des caract�ristiques et on y passe comme valeur les caract�ristiques
		 * de l'objet player
		 */
		labelPlayerAttackValue.setText(String.valueOf(player.getAttack()));
		labelPlayerLifeValue.setText(String.valueOf(player.getLife() + " / " + player.getMaxLife()));
		labelPlayerExperienceValue.setText(String.valueOf(player.getExperience()) + " / " + String.valueOf(player.getMaxEperience()) );
		labelPlayerLevelValue.setText(String.valueOf(player.getLevel()));
		
	}
	
	public String isLoot(Personnage player, Map isgrailMap, Config config) {
		/*
		 * IsLoot est une m�thode qui g�n�re du LOOT al�atoirement en fonction de la difficult� de la
		 * partie. Cela g�n�re une potion, qui r�g�ne al�atoirement des PV. Ces pv sont attribu�s au 
		 * joueur grace � la m�thode drinkPotion()
		 */
		Random random = new Random();
		int isPotion = random.generateRandomNumber(isgrailMap.getMinLootPotion(), isgrailMap.getMaxLootPotion());
		if(isPotion == 1) {
			config.displayAudio(config.getAudioPathPotion());
			int lifeGenerated = random.generateRandomNumber(1, isgrailMap.getMaxHealPotion());;
			Potion potion = new Potion(lifeGenerated);
			String returnStatus = player.drinkPotion(potion, player);
			return "\n En tuant le montre, tu as trouv� une potion! \n"
			+ returnStatus;
		}
		return "";
	}
	
}
