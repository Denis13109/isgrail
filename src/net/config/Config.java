package net.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.print.attribute.standard.Media;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import net.main.Main;

public class Config {
	//Audios
	private String audioPathLauncher ="ressource\\audios\\oblivion.wav";
	private String audioPathLOTRSnes ="ressource\\audios\\snesLOTR.wav";
	private String audioPathSword ="ressource\\audios\\sword.wav";
	private String audioPathWalk ="ressource\\audios\\walk.wav";
	private String audioPathGrrr ="ressource\\audios\\grrr.wav";
	private String audioPathFoward ="ressource\\audios\\foward.wav";
	private String audioPathSearch ="ressource\\audios\\search.wav";
	private String audioPathKfc ="ressource\\audios\\monster\\kfc.wav";
	private String audioPathCyclop ="ressource\\audios\\monster\\cyclop.wav";
	private String audioPathMinotaur ="ressource\\audios\\monster\\minotaur.wav";
	private String audioPathDryad ="ressource\\audios\\monster\\dryad.wav";
	private String audioPathCentaur ="ressource\\audios\\monster\\centaur.wav";
	private String audioPathChimere ="ressource\\audios\\monster\\chimere.wav";
	private String audioPathDrake ="ressource\\audios\\monster\\drake.wav";
	private String audioPathMedusa ="ressource\\audios\\monster\\meduse.wav";
	private String audioPathArmor ="ressource\\audios\\object\\armor.wav";
	private String audioPathAxe ="ressource\\audios\\object\\axe.wav";
	private String audioPathBow ="ressource\\audios\\object\\bow.wav";
	private String audioPathFlower ="ressource\\audios\\object\\flower.wav";
	private String audioPathShield ="ressource\\audios\\object\\shield.wav";
	private String audioPathPotion ="ressource\\audios\\object\\potion.wav";
	private String audioPathGameOver = "ressource\\audios\\gameover.wav";
	private String audioPathParade = "ressource\\audios\\parade.wav";
	private String audioPathHurtMale1 = "ressource\\audios\\hurtMale1.wav";
	private String audioPathHurtMale2 = "ressource\\audios\\hurtMale2.wav";
	private String audioPathHurtMale3 = "ressource\\audios\\hurtMale3.wav";
	private String audioPathHurtMale4 = "ressource\\audios\\hurtMale4.wav";
	private String audioPathHurtMale5 = "ressource\\audios\\hurtMale5.wav";
	private String audioPathHurtMale6 = "ressource\\audios\\hurtMale6.wav";
	private String audioPathLevelUp = "ressource\\audios\\levelup.wav";

	//Audios
	private String imagePathScroll = "ressource\\images\\scroll.png";
	private String imagePathMap = "ressource\\images\\wp.jpg";
	private String imagePathDenis = "ressource\\images\\monster\\denis.png";
	private String imagePathLouis = "ressource\\images\\monster\\louis.png";
	private String imagePathNevot = "ressource\\images\\monster\\kebab.png";
	private String imagePathChimera = "ressource\\images\\monster\\chimera.png";
	private String imagePathDrake = "ressource\\images\\monster\\drake.png";
	private String imagePathDryad = "ressource\\images\\monster\\dryad.png";
	private String imagePathCentaur = "ressource\\images\\monster\\centaur.png";
	private String imagePathCyclop = "ressource\\images\\monster\\cyclop.png";
	private String imagePathMinotaur = "ressource\\images\\monster\\minotaur.png";
	private String imagePathMedusa = "ressource\\images\\monster\\medusa.png";
	private String imagePathPlayer = "ressource\\images\\player.png";
	
	private String videoPathGameOver = "ressource\\videos\\gameover.mp4";

	


	public String getAudioPathLevelUp() {
		return audioPathLevelUp;
	}

	public void setAudioPathLevelUp(String audioPathLevelUp) {
		this.audioPathLevelUp = audioPathLevelUp;
	}

	public String getAudioPathHurtMale1() {
		return audioPathHurtMale1;
	}

	public void setAudioPathHurtMale1(String audioPathHurtMale1) {
		this.audioPathHurtMale1 = audioPathHurtMale1;
	}

	public String getAudioPathHurtMale2() {
		return audioPathHurtMale2;
	}

	public void setAudioPathHurtMale2(String audioPathHurtMale2) {
		this.audioPathHurtMale2 = audioPathHurtMale2;
	}

	public String getAudioPathHurtMale3() {
		return audioPathHurtMale3;
	}

	public void setAudioPathHurtMale3(String audioPathHurtMale3) {
		this.audioPathHurtMale3 = audioPathHurtMale3;
	}

	public String getAudioPathHurtMale4() {
		return audioPathHurtMale4;
	}

	public void setAudioPathHurtMale4(String audioPathHurtMale4) {
		this.audioPathHurtMale4 = audioPathHurtMale4;
	}

	public String getAudioPathHurtMale5() {
		return audioPathHurtMale5;
	}

	public void setAudioPathHurtMale5(String audioPathHurtMale5) {
		this.audioPathHurtMale5 = audioPathHurtMale5;
	}

	public String getAudioPathHurtMale6() {
		return audioPathHurtMale6;
	}

	public void setAudioPathHurtMale6(String audioPathHurtMale6) {
		this.audioPathHurtMale6 = audioPathHurtMale6;
	}

	public String getAudioPathParade() {
		return audioPathParade;
	}

	public void setAudioPathParade(String audioPathParade) {
		this.audioPathParade = audioPathParade;
	}
	
	public String getAudioPathPotion() {
		return audioPathPotion;
	}

	public void setAudioPathPotion(String audioPathPotion) {
		this.audioPathPotion = audioPathPotion;
	}
	
	public String getVideoPathGameOver() {
		return videoPathGameOver;
	}

	public void setVideoPathGameOver(String videoPathGameOver) {
		this.videoPathGameOver = videoPathGameOver;
	}

	public String getAudioPathGameOver() {
		return audioPathGameOver;
	}

	public void setAudioPathGameOver(String audioPathGameOver) {
		this.audioPathGameOver = audioPathGameOver;
	}

	public String getImagePathPlayer() {
		return imagePathPlayer;
	}

	public void setImagePathPlayer(String imagePathPlayer) {
		this.imagePathPlayer = imagePathPlayer;
	}

	public String getImagePathMap() {
		return imagePathMap;
	}

	public void setImagePathMap(String imagePathMap) {
		this.imagePathMap = imagePathMap;
	}
	
	public String getImagePathScroll() {
		return imagePathScroll;
	}

	public void setImagePathScroll(String imagePathScroll) {
		this.imagePathScroll = imagePathScroll;
	}

	public String getImagePathDenis() {
		return imagePathDenis;
	}

	public void setImagePathDenis(String imagePathDenis) {
		this.imagePathDenis = imagePathDenis;
	}

	public String getImagePathLouis() {
		return imagePathLouis;
	}

	public void setImagePathLouis(String imagePathLouis) {
		this.imagePathLouis = imagePathLouis;
	}

	public String getImagePathNevot() {
		return imagePathNevot;
	}

	public void setImagePathNevot(String imagePathNevot) {
		this.imagePathNevot = imagePathNevot;
	}

	public String getImagePathChimera() {
		return imagePathChimera;
	}

	public void setImagePathChimera(String imagePathChimera) {
		this.imagePathChimera = imagePathChimera;
	}

	public String getImagePathDrake() {
		return imagePathDrake;
	}

	public void setImagePathDrake(String imagePathDrake) {
		this.imagePathDrake = imagePathDrake;
	}

	public String getImagePathDryad() {
		return imagePathDryad;
	}

	public void setImagePathDryad(String imagePathDryad) {
		this.imagePathDryad = imagePathDryad;
	}

	public String getImagePathCentaur() {
		return imagePathCentaur;
	}

	public void setImagePathCentaur(String imagePathCentaur) {
		this.imagePathCentaur = imagePathCentaur;
	}

	public String getImagePathCyclop() {
		return imagePathCyclop;
	}

	public void setImagePathCyclop(String imagePathCyclop) {
		this.imagePathCyclop = imagePathCyclop;
	}

	public String getImagePathMinotaur() {
		return imagePathMinotaur;
	}

	public void setImagePathMinotaur(String imagePathMinotaur) {
		this.imagePathMinotaur = imagePathMinotaur;
	}

	public String getImagePathMedusa() {
		return imagePathMedusa;
	}

	public void setImagePathMedusa(String imagePathMedusa) {
		this.imagePathMedusa = imagePathMedusa;
	}

	public String getAudioPathGrrr() {
		return audioPathGrrr;
	}

	public void setAudioPathGrrr(String audioPathGrrr) {
		this.audioPathGrrr = audioPathGrrr;
	}
	public String getAudioPathFoward() {
		return audioPathFoward;
	}

	public void setAudioPathFoward(String audioPathFoward) {
		this.audioPathFoward = audioPathFoward;
	}

	public String getAudioPathSearch() {
		return audioPathSearch;
	}

	public void setAudioPathSearch(String audioPathSearch) {
		this.audioPathSearch = audioPathSearch;
	}

	public String getAudioPathKfc() {
		return audioPathKfc;
	}

	public void setAudioPathKfc(String audioPathKfc) {
		this.audioPathKfc = audioPathKfc;
	}

	public String getAudioPathCyclop() {
		return audioPathCyclop;
	}

	public void setAudioPathCyclop(String audioPathCyclop) {
		this.audioPathCyclop = audioPathCyclop;
	}

	public String getAudioPathMinotaur() {
		return audioPathMinotaur;
	}

	public void setAudioPathMinotaur(String audioPathMinotaur) {
		this.audioPathMinotaur = audioPathMinotaur;
	}

	public String getAudioPathDryad() {
		return audioPathDryad;
	}

	public void setAudioPathDryad(String audioPathDryad) {
		this.audioPathDryad = audioPathDryad;
	}

	public String getAudioPathCentaur() {
		return audioPathCentaur;
	}

	public void setAudioPathCentaur(String audioPathCentaur) {
		this.audioPathCentaur = audioPathCentaur;
	}

	public String getAudioPathChimere() {
		return audioPathChimere;
	}

	public void setAudioPathChimere(String audioPathChimere) {
		this.audioPathChimere = audioPathChimere;
	}

	public String getAudioPathDrake() {
		return audioPathDrake;
	}

	public void setAudioPathDrake(String audioPathDrake) {
		this.audioPathDrake = audioPathDrake;
	}

	public String getAudioPathMedusa() {
		return audioPathMedusa;
	}

	public void setAudioPathMedusa(String audioPathMedusa) {
		this.audioPathMedusa = audioPathMedusa;
	}

	public String getAudioPathWalk() {
		return audioPathWalk;
	}

	public void setAudioPathWalk(String audioPathWalk) {
		this.audioPathWalk = audioPathWalk;
	}
	
	public String getAudioPathLauncher() {
		return audioPathLauncher;
	}

	public String getAudioPathSword() {
		return audioPathSword;
	}

	public void setAudioPathSword(String audioPathSword) {
		this.audioPathSword = audioPathSword;
	}

	public void setAudioPathLauncher(String audioPathLauncher) {
		this.audioPathLauncher = audioPathLauncher;
	}

	public String getAudioPathLOTRSnes() {
		return audioPathLOTRSnes;
	}

	public void setAudioPathLOTRSnes(String audioPathLOTRSnes) {
		this.audioPathLOTRSnes = audioPathLOTRSnes;
	}
	
	public String getAudioPathArmor() {
		return audioPathArmor;
	}

	public void setAudioPathArmor(String audioPathArmor) {
		this.audioPathArmor = audioPathArmor;
	}
	
	public String getAudioPathAxe() {
		return audioPathAxe;
	}

	public void setAudioPathAxe(String audioPathAxe) {
		this.audioPathAxe = audioPathAxe;
	}
	
	public String getAudioPathBow() {
		return audioPathBow;
	}

	public void setAudioPathBow(String audioPathBow) {
		this.audioPathBow = audioPathBow;
	}
	
	public String getAudioPathFlower() {
		return audioPathFlower;
	}

	public void setAudioPathFlower(String audioPathFlower) {
		this.audioPathFlower = audioPathFlower;
	}
	
	public String getAudioPathShield() {
		return audioPathShield;
	}

	public void setAudioPathShield(String audioPathShield) {
		this.audioPathShield = audioPathShield;
	}

	public String getImagePath() {
		return imagePathScroll;
	}

	public void setImagePath(String imagePath) {
		this.imagePathScroll = imagePath;
	}

public Clip displayAudio(String audioPath) {
	try {
		File f = new File(audioPath);
	    AudioInputStream audioIn = AudioSystem.getAudioInputStream(f.toURI().toURL());  
	    Clip clip = AudioSystem.getClip();
	    clip.open(audioIn);
	    clip.start();
	    return clip;
      } catch (Exception e) {
        System.err.println("Une erreur est survenue avec l'audio : "+e.getMessage());
      }
	return null;
}

public void setImage(JFrame frameArg, String imagePath) {
	try {
    	ImageIcon imIcon = new ImageIcon(ImageIO.read(new File(imagePath)));
    	JLabel imageScroll = new JLabel(imIcon);
    	frameArg.setContentPane(imageScroll);
    } catch (IOException e) {
        e.printStackTrace();
    }
}

}
