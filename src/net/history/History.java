package net.history;

public class History {
	
	private int nbHistory = 1;

	private String history1 = "Bienvenue dans le monde d'Isgra�l ..\n "
			+ "Vous n'�tes pas cens� �tre ici ... *Hic* on a eu un \n "
			+ "probl�me dans les manipulations...Rah...";

	private String history2 = "C'est terrible tout �a ... Je .. Je .. .Je ne sais\n"
			+ " pas te ramener dans ton monde, j'en serai incapable..";

	private String history3 = "Quel est ton nom ?";
	
	private String history3b= "Tr�s heureuse de faire ta connaissance !\n"
			+ "Mon nom est Ath'ariel, je vis dans les bois non loin d'ici..\n";

	private String history4 = "Tu es ici dans la contr�e d'Erinsweid, une r�gion\n"
			+ "pas tr�s riche, que ce soit �conomiquement parlant ou bien au niveau \n"
			+ "des ressources.. La plupart des gens fuient cette r�gion ! \n"
			+ "Les pillages deviennent le quotidien des habitants par ici tu sais ..? \n"
			+ "Et encore ce n'est pas le pire !!";
	
	private String history5 = "Ils sont des milliers .. Ils ont envahit les terres,\n"
			+ "et ont �cras�les seuls hommes d'armes que l'empereur nous a envoy�.\n"
			+ "On parle bien de monstres sortis d'�tranges ab�mes form�es il y a peu.\n"
			+ "Depuis, c'est le chaos un peu partout..";

	private String history6 = "D'anciennes l�gendes racontentdans des temps recul�s,\n"
			+ "des monstres apparurent de nul part et r�pandirent le sang aux quatres \n"
			+ "coins du monde .. Orignal hein ? On se croirait encore dans un Anime Japonais \n"
			+ "pour ados pr�pub�res! Pourtant c'est bien notre r�alit�..";
	
	private String history7 = "J'ai .. Je .. Mon �me de femme de lettre m'inscita � �plucher \n"
			+ "des bouquins, manuscrits, lettres tout �crit qui me permettrait d'obtenir une r�ponse. \n"
			+ "A ma grande surprise, d'anciens rituels ont �t� pratiqu�s afin d'annihiler les monstres.\n";
	
	private String history8 = "Cepandant, leur manque de rigeur a caus� leur perte, et les \n"
			+ "rituels ont �chou�. De mes pauvres mains, j'ai repris ces savoirs archa�ques pour \n"
			+ "sauver notre civilisation. Mais .. Je dois te l'avouer, je suis d��ue et un peu d�prim�e.. \n"
			+ "Car c'est toi qui est apparu. Je .. Je m'attendais � mieux..";
	
	private String history9 = "J'avais pr�par� des �quipements pour cette grande occasion.. Tu peux les prendres\n"
			+ "Ils te sont destin�s! Tu y trouvera une �p�e avec laquelle tu pourras te d�fendre.\n"
			+ "Je dois retrouver les miens, le temps m'est compt�, je te souhaite une bonne aventure !!!\n"
			+ "Je prierai les 8 dieux pour toi, pour qu'il ne t'arrive rien !";
	
	private String informations = "[INFORMATIONS] - Le jeu d'Igra�l est un RPG tour par tour.\n"
			+ "Un compteur de cases sera affich� � votre �cran.\n"
			+ "Celui-ci indique le num�ro de la case sur laquelle \n"
			+ "vous vous trouvez. A chaque case, plusieurs options \n"
			+ "sont possibles. Vous pouvez fouiller la map sur \n"
			+ "laquelle vous vous trouver. Cependant cela vous fait \n"
			+ "consommer un tour, et un ennemi peut potentiellement\n"
			+ " vous attaquer. Vous pouvez trouver tout un tas d'\n"
			+ "�quipements, armes, armures, potions et bien d'autres \n"
			+ "objets qui vous aideront tout au long de votre aventure. \n"
			+ "Des monstrespeuvent vous attaquer s'ils se trouvent sur \n"
			+ "votre map, � vous de choisir si vous voulez les combattre \n"
			+ "ou bien fuire le combat. Survivre dans ce milieu hostile \n"
			+ "n'est pas chose ais�e, je vous souhaite bonne chance dans\n"
			+ " votre qu�te ! Au revoir aventurier !\n"
			+ "         --- Clique sur [START] ---";
	
	private String wrongName ="Hein ? Mais c'est pas un nom �a !!\n"
			+ "Un nom doit avoir plus de trois lettres m'enfin !!";
	
	public String getHistory3b() {
		return history3b;
	}

	public void setHistory3b(String history3b) {
		this.history3b = history3b;
	}
	
	public String getWrongName() {
		return wrongName;
	}

	public void setWrongName(String wrongName) {
		this.wrongName = wrongName;
	}

	public String getInformations() {
		return informations;
	}

	public void setInformations(String informations) {
		this.informations = informations;
	}

	public String getHistory9() {
		return history9;
	}

	public void setHistory9(String history9) {
		this.history9 = history9;
	}

	public String getHistory8() {
		return history8;
	}

	public void setHistory8(String history8) {
		this.history8 = history8;
	}

	public String getHistory7() {
		return history7;
	}

	public void setHistory7(String history7) {
		this.history7 = history7;
	}

	public String getHistory6() {
		return history6;
	}

	public void setHistory6(String history6) {
		this.history6 = history6;
	}

	public String getHistory5() {
		return history5;
	}

	public void setHistory5(String history5) {
		this.history5 = history5;
	}

	public String getHistory4() {
		return history4;
	}

	public void setHistory4(String history4) {
		this.history4 = history4;
	}

	public int getNbHistory() {
		return nbHistory;
	}

	public void setNbHistory(int nbHistory) {
		this.nbHistory = nbHistory;
	}
	
	public String getHistory2() {
		return history2;
	}

	public void setHistory2(String history2) {
		this.history2 = history2;
	}

	public String getHistory3() {
		return history3;
	}

	public void setHistory3(String history3) {
		this.history3 = history3;
	}

	public String getHistory1() {
		return history1;
	}

	public void setHistory1(String history1) {
		this.history1 = history1;
	}
	
}
