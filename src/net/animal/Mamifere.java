package net.animal;

public abstract class Mamifere extends Animal {

    public Mamifere(String name, char sex, int weight, int size) {
        super(name, sex, weight, size);
    }

    public void Accoucher() {
        if(super.getSex() == 'f') {
            System.out.println("C'est g�nial, non ? " + super.getName() + " a mit bas !");
        }
    }
    
    public void vagabonder() {
        System.out.println(super.getName() + " vagabonde");
    }
}
