package net.animal;

public class Tigres extends Mamifere implements AnimalTerrestre{

    public Tigres(String name, char sex, int weight, int size) {
        super(name, sex, weight, size);
    }

    public void walk() {
        System.out.println(super.getName() + " vagabonde pepouse dans la cambrousse !");
    }
}