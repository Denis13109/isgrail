package net.animal;

public class Loups extends Mamifere implements AnimalTerrestre{

    public Loups(String name, char sex, int weight, int size) {
        super(name, sex, weight, size);
    }

    public void walk() {
        System.out.println(super.getName() + " vagabonde pepouse dans la cambrousse !");
    }
}