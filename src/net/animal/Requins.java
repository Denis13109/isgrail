package net.animal;

public class Requins extends Animal implements AnimalMarin {

    public Requins(String name, char sex, int weight, int size) {
        super(name, sex, weight, size);
    }

    @Override
    public void swim() {
        System.out.println(super.getName() + " guette sa proie");
    }
}