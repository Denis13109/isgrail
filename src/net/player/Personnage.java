package net.player;

import javax.swing.JLabel;
import javax.swing.JTextArea;

import net.config.Config;
import net.monster.Monster;
import net.object.Potion;
import net.random.Random;

public class Personnage {

	private String name;
	private int life;
	private int attack;
	private int maxLife;
	private int level;
	private int experience;
	private int maxEperience;
	private int experienceEachLevelUp = 200;


	public Personnage(String name, int life, int attack, int level) {
		super();
		this.name = name;
		this.life = life;
		this.attack = attack;
		this.maxLife = life;
		this.level = level;
	}
	


	public int getExperienceEachLevelUp() {
		return experienceEachLevelUp;
	}


	public void setExperienceEachLevelUp(int experienceEachLevelUp) {
		this.experienceEachLevelUp = experienceEachLevelUp;
	}
	
	public int getMaxEperience() {
		return maxEperience;
	}


	public void setMaxEperience(int maxEperience) {
		this.maxEperience = maxEperience;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public int getMaxLife() {
		return maxLife;
	}

	public void setMaxLife(int maxLife) {
		this.maxLife = maxLife;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}
	public boolean attackMonster(Monster target) {
		//M�thode qui attaque une target, ici un monstre et lui enl�ve de la vie ou non
		// Si le joueur touche le monstre, alors on retourne true, sinon false
		Random random = new Random();
		int randomAttack = random.generateRandomNumber(0, attack);
		if(randomAttack == 0) {
			return false;
		}
		target.setLife(target.getLife() - randomAttack);
		return true;
	}
	
	public String drinkPotion(Potion potion, Personnage player) {
		/*
		 * M�thode permet au joueur de boire une potion.
		 * Un nombre al�atoire est g�n�r� pour le nombre de PV restaur�.
		 * Un check est pr�sent pour v�rifier que la potion n'overHeal pas.
		 * La vie du joueur est ensuite mise � jour
		 */
		
		int heal = potion.getHealth();
		player.setLife(player.getLife() + heal);
		if(player.maxLife < player.getLife()) {
			player.setLife(player.getMaxLife());
			return "Ta vie est au maximum. \n";
		}
		return "Tu viens de te soigner " + potion.getHealth() + " de points de vie. \n";
	}
	
	public void getExeperienced(Personnage player, int xp, JTextArea txt) {
		/*
		 * M�thode qui permet au joueur de prendre de l'exp�rience.
		 * Un check est pr�sent pour v�rifier que le joueur ne d�passe pas sa barre d'xp.
		 * S'il la d�passe, le surplus et conserv�. Le joueur prend un niveau, et le surplus est
		 * Ajout� � la nouvelle barre d'exp�rience.
		 * Les caract�ristiques du joueur sont ensuite mises � jour.
		 */
		player.setExperience(player.getExperience() + xp);
		if(player.getExperience() >= player.getMaxEperience()) {
			Config config = new Config();
			config.displayAudio(config.getAudioPathLevelUp());
			int overXp = player.getExperience() - player.getMaxEperience();
			player.setLevel(player.getLevel() + 1);
			txt.setText("\n Bravo !! Tu viens de gagner un niveau ! \n");
			player.setMaxEperience(player.getMaxEperience() + experienceEachLevelUp);
			player.setExperience(overXp);
			player.setLife(maxLife);
			return;
		}
		txt.setText("\n Tu as gagn� " + xp + "xp. \n");
	}
}
