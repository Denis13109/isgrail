package net.map;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import net.config.Config;
import net.monster.*;
import net.object.GenerationItems;
import net.object.Potion;
import net.object.Stuff;
import net.random.Random;

public class Map {

	private final int MAPTOT = 100;
	private int PositionPlayer = 0;
	private int minSpawnMonster;
	private int maxSpawnMonster;
	private int minSpawnMonsterLife;
	private int maxSpawnMonsterLife;
	private int minSpawnMonsterAttack;
	private int maxSpawnMonsterAttack;
	
	private int minSpawnItems;
	private int maxSpawnItems;
	private int minItemsLife;
	private int maxItemsLife;
	private int minItemsAttack;
	private int maxItemsAttack;
	
	private int minSpawnMonsterName = 1;
	private int maxSpawnMonsterName = 10;
	private int minSpawnItemsName = 1;
	private int maxSpawnItemsName = 10;

	private int maxLootPotion;
	private int minLootPotion;
	private int maxHealPotion;

	private int maxEperienceAgainMonster;
	
	
	
	
	public int getMaxEperienceAgainMonster() {
		return maxEperienceAgainMonster;
	}

	public void setMaxEperienceAgainMonster(int maxEperienceAgainMonster) {
		this.maxEperienceAgainMonster = maxEperienceAgainMonster;
	}
	
	public int getMaxHealPotion() {
		return maxHealPotion;
	}

	public void setMaxHealPotion(int maxHealPotion) {
		this.maxHealPotion = maxHealPotion;
	}

	public int getMinLootPotion() {
		return minLootPotion;
	}

	public void setMinLootPotion(int minLootPotion) {
		this.minLootPotion = minLootPotion;
	}

	public int getMaxLootPotion() {
		return maxLootPotion;
	}

	public void setMaxLootPotion(int maxLootPotion) {
		this.maxLootPotion = maxLootPotion;
	}
	
	public int getMinItemsLife() {
		return minItemsLife;
	}

	public void setMinItemsLife(int minItemsLife) {
		this.minItemsLife = minItemsLife;
	}

	public int getMaxItemsLife() {
		return maxItemsLife;
	}

	public void setMaxItemsLife(int maxItemsLife) {
		this.maxItemsLife = maxItemsLife;
	}

	public int getMinItemsAttack() {
		return minItemsAttack;
	}

	public void setMinItemsAttack(int minItemsAttack) {
		this.minItemsAttack = minItemsAttack;
	}

	public int getMaxItemsAttack() {
		return maxItemsAttack;
	}

	public void setMaxItemsAttack(int maxItemsAttack) {
		this.maxItemsAttack = maxItemsAttack;
	}

	public int getMinSpawnItems() {
		return minSpawnItems;
	}

	public void setMinSpawnItems(int minSpawnItems) {
		this.minSpawnItems = minSpawnItems;
	}

	public int getMaxSpawnItems() {
		return maxSpawnItems;
	}

	public void setMaxSpawnItems(int maxSpawnItems) {
		this.maxSpawnItems = maxSpawnItems;
	}

	
	public int getMinSpawnMonsterLife() {
		return minSpawnMonsterLife;
	}

	public void setMinSpawnMonsterLife(int minSpawnMonsterLife) {
		this.minSpawnMonsterLife = minSpawnMonsterLife;
	}

	public int getMaxSpawnMonsterLife() {
		return maxSpawnMonsterLife;
	}

	public void setMaxSpawnMonsterLife(int maxSpawnMonsterLife) {
		this.maxSpawnMonsterLife = maxSpawnMonsterLife;
	}

	public int getMinSpawnMonsterAttack() {
		return minSpawnMonsterAttack;
	}

	public void setMinSpawnMonsterAttack(int minSpawnMonsterAttack) {
		this.minSpawnMonsterAttack = minSpawnMonsterAttack;
	}

	public int getMaxSpawnMonsterAttack() {
		return maxSpawnMonsterAttack;
	}

	public void setMaxSpawnMonsterAttack(int maxSpawnMonsterAttack) {
		this.maxSpawnMonsterAttack = maxSpawnMonsterAttack;
	}

	public int getMinSpawnMonster() {
		return minSpawnMonster;
	}

	public void setMinSpawnMonster(int minSpawnMonster) {
		this.minSpawnMonster = minSpawnMonster;
	}

	public int getMaxSpawnMonster() {
		return maxSpawnMonster;
	}

	public void setMaxSpawnMonster(int maxSpawnMonster) {
		this.maxSpawnMonster = maxSpawnMonster;
	}

	public Map() {
	}

	public int getPositionPlayer() {
		return PositionPlayer;
	}

	public void setPositionPlayer(int positionPlayer) {
		PositionPlayer = positionPlayer;
	}

	public int getMAPTOT() {
		return MAPTOT;
	}
	
	public Monster spawn(JLabel labelImageMonster, Config config) {
		/*
		 * M�thode qui fait spawn un Monstre
		 */
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		Random random = new Random();
		int randomNum = random.generateRandomNumber(minSpawnMonster, maxSpawnMonster);
		if(randomNum == 1) {
			//Ici cr�ation d'icon (Ajout d'images pour l'apparition du monstre.).
			//Si un monstre spawn, on ajoute l'icone au Label Monstre pass� en param�tre.
			//Cela fera afficher l'image au joueur
			ImageIcon centaurImg = new ImageIcon(config.getImagePathCentaur());
		    ImageIcon minotaurImg = new ImageIcon(config.getImagePathMinotaur());
		    ImageIcon chimeraImg = new ImageIcon(config.getImagePathChimera());
		    ImageIcon medusaImg = new ImageIcon(config.getAudioPathMedusa());
		    ImageIcon cyclopImg = new ImageIcon(config.getImagePathCyclop());
		    ImageIcon denisImg = new ImageIcon(config.getImagePathDenis());
		    ImageIcon louisImg = new ImageIcon(config.getImagePathLouis());
		    ImageIcon nevotImg = new ImageIcon(config.getImagePathNevot());
		    ImageIcon dryadImg = new ImageIcon(config.getImagePathDryad());
		    ImageIcon drakeImg = new ImageIcon(config.getImagePathDrake());
		    
			int randomNumLife = random.generateRandomNumber(minSpawnMonsterLife, maxSpawnMonsterLife);
			int randomNumAttack = random.generateRandomNumber(minSpawnMonsterAttack, maxSpawnMonsterAttack);
			int randomMonster =  random.generateRandomNumber(minSpawnMonsterName, maxSpawnMonsterName);
			/*
			 * RandomMonster est un nombre int g�n�r� al�atoirement. En fonction de ce nombre,
			 * Diff�rents monstres sont retourn�s. Aujourd'hui, il y a 10 monstres, donc le nombre
			 * est g�n�r� entre 1 et 10. Si RandomMonster == 1	, alors un centaure apparaitra,
			 * et ainsi desuite.
			 */
			switch(randomMonster) {
			//Chaque monstre a un audio sp�cifique et est display en fonction du monstre
			case 1:
				Monster centaur = new Centaure(randomNumLife, randomNumAttack);
				config.displayAudio(config.getAudioPathCentaur());
				labelImageMonster.setIcon(centaurImg);
				labelImageMonster.setVisible(true);
				return centaur;
			case 2:
				Monster dryad = new Dryad(randomNumLife, randomNumAttack);
				config.displayAudio(config.getAudioPathDryad());
				labelImageMonster.setIcon(dryadImg);
				labelImageMonster.setVisible(true);
				return dryad;
			case 3:
				Monster drake = new Drake(randomNumLife, randomNumAttack);
				config.displayAudio(config.getAudioPathDrake());
				labelImageMonster.setIcon(drakeImg);
				labelImageMonster.setVisible(true);
				return drake;
			case 4:
				Monster minotaur = new Minotaur(randomNumLife, randomNumAttack);
				config.displayAudio(config.getAudioPathMinotaur());
				labelImageMonster.setIcon(minotaurImg);
				labelImageMonster.setVisible(true);
				return minotaur;
			case 5:
				Monster louis = new Louis(randomNumLife, randomNumAttack);
				labelImageMonster.setIcon(louisImg);
				labelImageMonster.setVisible(true);
				return louis;
			case 6:
				Monster denis = new Denis(randomNumLife, randomNumAttack);
				labelImageMonster.setIcon(denisImg);
				labelImageMonster.setVisible(true);
				return denis;
			case 7:
				Monster nevot = new Nevot(randomNumLife, randomNumAttack);
				config.displayAudio(config.getAudioPathKfc());
				labelImageMonster.setIcon(nevotImg);
		    	labelImageMonster.setVisible(true);
				return nevot;
			case 8:
				Monster cyclop = new Cyclop(randomNumLife, randomNumAttack);
				config.displayAudio(config.getAudioPathCyclop());
				labelImageMonster.setIcon(cyclopImg);
				labelImageMonster.setVisible(true);
				return cyclop;
			case 9:
				Monster medusa = new Medusa(randomNumLife, randomNumAttack);
				config.displayAudio(config.getAudioPathMedusa());
				labelImageMonster.setIcon(medusaImg);
				labelImageMonster.setVisible(true);
				return medusa;
			case 10:
				Monster chimera = new Chimera(randomNumLife, randomNumAttack);
				config.displayAudio(config.getAudioPathChimere());
				labelImageMonster.setIcon(chimeraImg);
				labelImageMonster.setVisible(true);
				return chimera;
				default:
			}
		}
		return null;
	}
	
	public Stuff stuffSpawn() {
		Random random = new Random();
		int randomNum = random.generateRandomNumber(minSpawnItems,maxSpawnItems);
		if(randomNum == 1) {
			int randomNumLife = random.generateRandomNumber(minItemsLife, maxItemsLife);
			int randomNumAttack = random.generateRandomNumber(minItemsAttack, maxItemsAttack);
			int randomNumName = random.generateRandomNumber(minSpawnItemsName, maxSpawnItemsName);
			GenerationItems generationItems = new GenerationItems();
			Stuff spawnStuff = new Stuff(generationItems.generateNameStuff(randomNumName),randomNumLife,randomNumAttack);
			return spawnStuff;
		}
		return null;
	}

}
