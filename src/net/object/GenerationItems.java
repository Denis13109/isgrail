package net.object;

import net.config.Config;

public class GenerationItems {

	public GenerationItems() {
		// TODO Auto-generated constructor stub
	}
	
	public String generateNameStuff(int numberStuff) {
		StuffType typeStuff;
		Config config = new Config();
		String StuffTypeString ="";
		
		switch(numberStuff) {
		case 1:
			typeStuff = StuffType.Chest;
			StuffTypeString = typeStuff.toString();
			config.displayAudio(config.getAudioPathArmor());
	        break;
		case 2:
			typeStuff = StuffType.Leggings;
			StuffTypeString = typeStuff.toString();
			config.displayAudio(config.getAudioPathArmor());
	        break;
	    case 3:
	    	typeStuff = StuffType.Boots;
	    	StuffTypeString = typeStuff.toString();
	    	config.displayAudio(config.getAudioPathArmor());
	    	break;
	    case 4:
	    	typeStuff = StuffType.Helmet;
	    	StuffTypeString = typeStuff.toString();
	    	config.displayAudio(config.getAudioPathArmor());
	    	break;
	    case 5:
	    	typeStuff = StuffType.Sword;
	    	StuffTypeString = typeStuff.toString();
	    	config.displayAudio(config.getAudioPathSword());
	    	break;
	    case 6:
	    	typeStuff = StuffType.Axe;
	    	StuffTypeString = typeStuff.toString();
	    	config.displayAudio(config.getAudioPathAxe());
	    	break;
	    case 7:
	    	typeStuff = StuffType.Bow;
	    	StuffTypeString = typeStuff.toString();
	    	config.displayAudio(config.getAudioPathBow());
	    	break;
	    case 8:
	    	typeStuff = StuffType.Mass;
	    	StuffTypeString = typeStuff.toString();
	    	config.displayAudio(config.getAudioPathSword());
	    	break;
	    case 9:
	    	typeStuff = StuffType.Flower;
	    	StuffTypeString = typeStuff.toString();
	    	config.displayAudio(config.getAudioPathFlower());
	    	break;
	    case 10:
	    	typeStuff = StuffType.Shield;
	    	StuffTypeString = typeStuff.toString();
	    	config.displayAudio(config.getAudioPathShield());
	    	break;
	    default:
	    	
		}
		
		return StuffTypeString;
	}

}
