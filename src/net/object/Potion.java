package net.object;

public class Potion {

	private int health;
	private int maxHealth;
	
	public Potion(int health) {
		
		this.health = health;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getMaxhealth() {
		return maxHealth;
	}

	public void setMaxhealth(int maxhealth) {
		this.maxHealth = maxhealth;
	}

}
