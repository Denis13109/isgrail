package net.object;

public class Stuff {

	private String name;
	private int life;
	private int attack;
	
	public Stuff(String name, int life, int attack) {
		this.setLife(life);
		this.setAttack(attack);
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}
}
