package net.random;

import java.util.concurrent.ThreadLocalRandom;

public class Random {

	public Random() {
		// TODO Auto-generated constructor stub
	}
	
	public int generateRandomNumber(int min, int max){
		
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int numberGenerate = ThreadLocalRandom.current().nextInt(min, max + 1);
		return numberGenerate;
	}
}

