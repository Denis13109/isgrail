package threads;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JTextArea;

import net.ihm.World;
import net.object.StuffType;

public class MyThread implements Runnable{

	JTextArea myTxtArea = null;
	
	@Override
	public void run() {
		String logs = "\n Voici la liste des items trouvables :\n ";
		List<StuffType> myList  = Arrays.asList(StuffType.values());
		for(StuffType myEnum : myList) {
			logs = logs + myEnum.toString() + "\n";
		}
		myTxtArea.append(logs);
	}
	
	public void myInfosSetted(JTextArea txtWorld) {
		this.myTxtArea = txtWorld;
	}
	
	

}
