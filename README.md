# Isgrail

Information relative à la réalisation du project-java.
Version de Java utilisée : 1.8.0_201

## Membres

- GAUTHIER Louis, louis.gauthier13111@gmail.com, 06 24 79 58 02
- TOUFFUT Denis, denis13109@gmail.com, 07 88 41 11 47


## Repository Gitlab

Le lien au repository Gitlab [ici](https://gitlab.com/Denis13109/isgrail)

## Description de l'archive

- L'archive a un volume supérieur à 10 Mo, car nous avons utilisé des audios.
- Les différentes ressources autres que le code sont disponibles dans le dossier ressource
- Le code est à la racine et vous trouverez le code principal dans le dossier src (c'est un projet Eclipse)
- L'application au format JAR est dans le dossier build
- La documentation générée avec JavaDoc est disponible dans le dossier Doc
- Le manuel d'utilisation est à la racine : c'est le fichier manuel.md

